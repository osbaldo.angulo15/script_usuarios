import React from 'react';
import { Query } from 'react-apollo';
import Del from './DeleteTodo';
import Mark from './MarkTodo';
import { TODO } from './queries';


const Todos = () => {

  return (
    <div style= {{backgroundColor: "pink"}} >
      <Query query={TODO}>
        {({ loading, error, data }) => {
          if (loading) return "Loading...";
          if (error) return (console.log(error));
          if (data) {
            return data.municipios.map((element) => (
              <div key={element.id}>
                <span key={element.id} > {element.nombre}  <Del id={element.id} /> <Mark id={element.id} /> </span>
              </div>
            ));
          }
          else {
            return (
            <div> get some work boi...</div>
            )
          }
        }}

      </Query>
    </div>
  )
}

export default Todos;