import XLSX from 'xlsx';

module.exports = {
  getData: (file) => {
    const workbook = XLSX.readFile(file);
    const sheet_name_list = workbook.SheetNames;
    let data = [];
    sheet_name_list.forEach((y) => {
      let worksheet = workbook.Sheets[y];
      let headers = {};
      for (let z; z<worksheet.length; z++) {
        if (z[0] === '!') continue;
        let tt = 0;
        for (let i = 0; i < z.length; i++) {
          if (!isNaN(z[i])) {
            tt = i;
            break;
          }
        };
        const col = z.substring(0, tt);
        const row = parseInt(z.substring(tt));
        const value = worksheet[z].v;

        //store header names
        if (row === 1 && value) {
          headers[col] = value;
          continue;
        }

        if (!data[row]) data[row] = {};
        data[row][headers[col]] = value;
      }
      //drop those first two rows which are empty
      data.shift();
      data.shift();
    });
    return data;
  }
}