//@flow
import React from 'react';
import { Mutation } from 'react-apollo';
import { ALUMNOSQUERY, DELETE_ALUMNOS } from './queries';

const DeleteAlumno = (id) => {
  return <div> 
      <Mutation mutation={DELETE_ALUMNOS}>
            {(deleteAlumnos, { data }) => (
                <button onClick={() => 
                    deleteAlumnos({
                        variables: id, refetchQueries: [{ query: ALUMNOSQUERY }] 
                       })
                }>x</button>
            )}
        </Mutation>
  </div>;
};

export default (DeleteAlumno);