import gql from 'graphql-tag';

export const ALUMNOSQUERY = gql`
    query {
        usuarios {
            id
            apellido_materno
            apellido_paterno
            ciclo_escolar_id
            correo
            fecha_registro
            registrado_por
            nombre
            tipo_usuario
        }
    }
  
`;

export const ADD_ALUMNOS = gql`
mutation addAlumnos(
    $apellidoMaterno: String!,
    $apellidoPaterno: String!,
    $correo: String!,
    $nombre: String!,
    ) {
    insert_usuarios(objects: [{
        apellido_materno: $apellidoMaterno,
        apellido_paterno: $apellidoPaterno,
        correo: $correo,
        nombre: $nombre,
        fecha_registro: "2019-10-23",
        ciclo_escolar_id: 1,
        tipo_usuario: 2,
    }]){
      returning{
        id
      }
    }
}
`;
export const DELETE_ALUMNOS = gql`
mutation deleteAlumnos($id: Int!) {
    delete_usuarios(
        where: {id: {_eq: $id}})
      {
        affected_rows
      }
}

`;