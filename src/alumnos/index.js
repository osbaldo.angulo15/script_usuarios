//@flow
import React from 'react';
import { Query, Mutation } from 'react-apollo';
import { ALUMNOSQUERY, ADD_ALUMNOS, } from './queries';
import DeleteAlumno from './delete';
import AddPerfil from '../Perfil/addprofile';
import _ from 'lodash';
import { alumnos } from './constants';
const Alumnos = () => {
    const filterByCurp = (correo) => {
        let curp = correo.replace('@sepyc.edu.mx', '')
        return _.filter(alumnos, { curpAlumno: curp })
    }
    const setAlumnos = (addAlumnos) => {
        alumnos.forEach(element => {
            addAlumnos({
                variables: {
                    apellidoMaterno: element.maternoAlumno,
                    apellidoPaterno: element.paternoAlumno,
                    correo: `${element.curpAlumno}@sepyc.edu.mx`,
                    nombre: element.nombreAlumno,
                }, refetchQueries: [{ query: ALUMNOSQUERY }]
            })

        });
    }
    return <div>
        <Mutation mutation={ADD_ALUMNOS}>
            {(addTodo, { data }) => (
                <button onClick={() => setAlumnos(addTodo)}>click</button>
            )}
        </Mutation>
        <Query query={ALUMNOSQUERY}>
            {({ loading, error, data }) => {
                if (loading) return "Loading...";
                if (error) return (console.log(error));
                if (data) {
                    return data.usuarios.map((element) => (
                        <div key={element.id}>
                            <table>
                                <tr>
                                    <th>
                                        <span key={element.id} >{element.id}</span>
                                    </th>
                                    <th>
                                        <span key={element.id} >{element.correo}</span>
                                    </th>
                                    <th>
                                        <DeleteAlumno id={element.id}></DeleteAlumno>
                                    </th>
                                    <th>
                                        <AddPerfil
                                            id={element.id}
                                            clave={
                                                filterByCurp(element.correo).length !== 0 ? filterByCurp(element.correo)[0].claveCT : null
                                            }
                                            nombreGrupo={
                                                filterByCurp(element.correo).length !== 0 ? filterByCurp(element.correo)[0].claveGrupo : null
                                            }
                                            grado={
                                                filterByCurp(element.correo).length !== 0 ? filterByCurp(element.correo)[0].claveGrado : null
                                            }
                                        ></AddPerfil>
                                    </th>
                                </tr>
                            </table>
                        </div>
                    ));
                }
                else {
                    return (
                        <div> get some work boi...</div>
                    )
                }
            }}
        </Query>
    </div>;
};

export default (Alumnos);