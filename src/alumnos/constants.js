export const alumnos = [
	{
		"No_registro": "5119",
		"curpAlumno": "AERL121125MSRRLSA3",
		"paternoAlumno": "ARMENTA",
		"maternoAlumno": "RUELAS",
		"nombreAlumno": "LESLI VIOLETA",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5120",
		"curpAlumno": "AOIG121205HSLCBRA5",
		"paternoAlumno": "ACOSTA",
		"maternoAlumno": "IBARRA",
		"nombreAlumno": "JOSE GERARDO",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5121",
		"curpAlumno": "AULN120601MSLGZMA2",
		"paternoAlumno": "AGUIRRE",
		"maternoAlumno": "LOZOYA",
		"nombreAlumno": "NAOMY LEONOR",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5122",
		"curpAlumno": "AUOS121229HSLNLBA6",
		"paternoAlumno": "ANGULO",
		"maternoAlumno": "OLIVAS",
		"nombreAlumno": "SEBASTIAN",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5123",
		"curpAlumno": "BAGN120113MSLRCTA0",
		"paternoAlumno": "BARRAZA",
		"maternoAlumno": "GUICHO",
		"nombreAlumno": "NATALIA YAQUELIN",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5124",
		"curpAlumno": "BOLJ120209MSLRYNA9",
		"paternoAlumno": "BORQUEZ",
		"maternoAlumno": "LEYVA",
		"nombreAlumno": "JANETH GUADALUPE",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5125",
		"curpAlumno": "BOPE120909HSLNNDA0",
		"paternoAlumno": "BON",
		"maternoAlumno": "PONCE",
		"nombreAlumno": "EDUARDO HIPOLITO",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5126",
		"curpAlumno": "CALA120729HSLRPXA9",
		"paternoAlumno": "CARRASCO",
		"maternoAlumno": "LOPEZ",
		"nombreAlumno": "AXEL SEBASTIAN",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5127",
		"curpAlumno": "CUSN120608MSLVNHA2",
		"paternoAlumno": "CUEVAS",
		"maternoAlumno": "SAINZ",
		"nombreAlumno": "NAHOMI GUADALUPE",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5128",
		"curpAlumno": "FEAF120824MSLLGRA5",
		"paternoAlumno": "FELIX",
		"maternoAlumno": "AGUILAR",
		"nombreAlumno": "MARIA FERNANDA",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5129",
		"curpAlumno": "FEVS121015HSLLLNA5",
		"paternoAlumno": "FELIX",
		"maternoAlumno": "VALVERDE",
		"nombreAlumno": "SANTIAGO",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5130",
		"curpAlumno": "GARA121216HSLLMLA5",
		"paternoAlumno": "GALVEZ",
		"maternoAlumno": "RAMIREZ",
		"nombreAlumno": "ALEXIS",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5131",
		"curpAlumno": "HEBV120705MSLRJLA4",
		"paternoAlumno": "HERRERA",
		"maternoAlumno": "BOJORQUEZ",
		"nombreAlumno": "VALERIA NOHEMI",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5132",
		"curpAlumno": "HEWA120104MSLRVLA5",
		"paternoAlumno": "HERNANDEZ",
		"maternoAlumno": "WVIARCO",
		"nombreAlumno": "ALEXIA ISAIRIS",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5133",
		"curpAlumno": "LOAC120527MSLZCLA3",
		"paternoAlumno": "LOZOYA",
		"maternoAlumno": "ACOSTA",
		"nombreAlumno": "CLAUDIA ELIZABETH",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5134",
		"curpAlumno": "LOLC120825HSLPPRA5",
		"paternoAlumno": "LOPEZ",
		"maternoAlumno": "LOPEZ",
		"nombreAlumno": "CARLOS ALONSO",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5135",
		"curpAlumno": "MIZD120918MSLRZYA2",
		"paternoAlumno": "MIRAMONTES",
		"maternoAlumno": "ZAZUETA",
		"nombreAlumno": "DAYANA GUADALUPE",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5136",
		"curpAlumno": "MOOE121216MSLRCRA4",
		"paternoAlumno": "MORENO",
		"maternoAlumno": "OCHOA",
		"nombreAlumno": "ERIKA GUADALUPE",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5137",
		"curpAlumno": "MOPI120307MSLNNNA6",
		"paternoAlumno": "MONZON",
		"maternoAlumno": "PONCE",
		"nombreAlumno": "INGRID ISABELLA",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5138",
		"curpAlumno": "OICJ120326HSLLRSA1",
		"paternoAlumno": "OLIVAS",
		"maternoAlumno": "CORTEZ",
		"nombreAlumno": "JESUS ALEXANDER",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5139",
		"curpAlumno": "OUAI120928MSLSGVA2",
		"paternoAlumno": "OSUNA",
		"maternoAlumno": "AGUIRRE",
		"nombreAlumno": "IVANA ANDREA",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5140",
		"curpAlumno": "POCJ121123HSLNRSA1",
		"paternoAlumno": "PONCE",
		"maternoAlumno": "CORTEZ",
		"nombreAlumno": "JESUS DAVID",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5141",
		"curpAlumno": "POIP110424MSLNBRA4",
		"paternoAlumno": "PONCE",
		"maternoAlumno": "IBARRA",
		"nombreAlumno": "PERLA RUBI",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5142",
		"curpAlumno": "RASA121126HSLMLNA7",
		"paternoAlumno": "RAMIREZ",
		"maternoAlumno": "SILVAS",
		"nombreAlumno": "ANGEL AYDAN",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5143",
		"curpAlumno": "REMA120501HSLYDLA7",
		"paternoAlumno": "REYES",
		"maternoAlumno": "MEDINA",
		"nombreAlumno": "ALAN YAMIR",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5144",
		"curpAlumno": "RESE121228MSLYLVA8",
		"paternoAlumno": "REYES",
		"maternoAlumno": "SOLANO",
		"nombreAlumno": "EVELYN ITZEL",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5145",
		"curpAlumno": "ROAL120229MSLMNTA6",
		"paternoAlumno": "ROMAN",
		"maternoAlumno": "ANGULO",
		"nombreAlumno": "LITZI MONSERRAT",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5146",
		"curpAlumno": "ROSJ120708MSLMNBA3",
		"paternoAlumno": "ROMERO",
		"maternoAlumno": "SAINZ",
		"nombreAlumno": "JOBHANA IVETH",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5147",
		"curpAlumno": "SAAC121130MSLNLRA7",
		"paternoAlumno": "SANCHEZ",
		"maternoAlumno": "ALARCON",
		"nombreAlumno": "CRISTA SAYURI",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	},
	{
		"No_registro": "5148",
		"curpAlumno": "ZAZM121217HSLZMRA2",
		"paternoAlumno": "ZAZUETA",
		"maternoAlumno": "ZAMUDIO",
		"nombreAlumno": "JOSE MARIA",
		"descripcionCicloEscolar": "2018-2019",
		"claveCT": "25EPR0032X",
		"centroTrabajo": "GENERAL IGNACIO ZARAGOZA",
		"claveNivelEducativo": "2",
		"nivelEducativo": "PRIMARIA",
		"claveGrado": "1",
		"claveGrupo": "A",
		"claveTurno": "200",
		"turno": "VESPERTINO"
	}
]