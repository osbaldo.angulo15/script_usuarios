import React, { Component } from 'react';
import './App.css';
import ApolloClient from "apollo-boost";
import {ApolloProvider} from "react-apollo";
import Alumnos from "./alumnos/index";
import Perfil from "./Perfil/index";


const client = new ApolloClient({
  uri: "https://api.escuela.work/v1/graphql",
  headers: {"x-hasura-admin-secret": "terceratemporada"},
});


class App extends Component {
	render() {
    return (
        <ApolloProvider client={client}>
        <div className="todo-app container">
          <h1  className=" title center blue-text">Todos</h1>
          <Alumnos />
          <Perfil hola={7}></Perfil>
        </div>
        
        </ApolloProvider>
    );
  }
}
export default App;
