//@flow
import React, { useEffect, useState, } from 'react';
import { Query, Mutation } from 'react-apollo';
import { PERFILESQUERY, ADD_PERFIL, ZONAS_ESCOLARES, USUARIOID } from './queries';
import DeleteAlumno from './delete';
import { alumnos } from '../alumnos/constants';
import { useQuery } from '@apollo/react-hooks';
import { ALUMNOSQUERY } from '../alumnos/queries';

const Perfil = () => {
    //user id hooks
    const [userId, setuserId] = useState(0);
    //use efect
    useEffect(() => {
        //   refetch(userId);
    }, [userId]);
    //modifica user id
    //llamando query usuarios
    const { 
        called: calledUsuarios,
        loading: loadingUsuarios,
        data: usuarios 
    } = useQuery(ALUMNOSQUERY);
    //llamando query de Perfil
    const { 
        called, 
        loading, 
        data: dataPerfilByUsuarioId 
    } = useQuery(
        USUARIOID, 
        { variables: { id: userId } 
    });
    // FUNCION PARA HACER MUTACIONES
    const setAlumnos = async (hola) => {
        if(hola === usuarios.usuarios.length){
            setuserId(hola);

        }else{
            setuserId(hola);
            setAlumnos(hola+1)

        }

            /* addAlumnos({
                variables: {
                    apellidoMaterno: element.maternoAlumno,
                    apellidoPaterno: element.paternoAlumno,
                    correo: `${element.curpAlumno}@sepyc.edu.mx`,
                    nombre: element.nombreAlumno,
                }, refetchQueries: [{ query: PERFILESQUERY }]
            }) */

    }


    return <div>
        <Query query={PERFILESQUERY}>
            {({ loading, error, data }) => {
                if (loading) return "Loading...";
                if (error) return (console.log(error));
                if (data) {
                    return data.perfil.map((element) => (
                        <div style={{ backgroundColor: 'pink' }} key={element.id}>
                            <span key={element.id} >{`${element.id} usuarioID: ${element.usuario_id}`}</span>
                        </div>
                    ));
                }
                else {
                    return (
                        <div> get some work boi...</div>
                    )
                }
            }}
        </Query>
    </div>;
};

export default (Perfil);