//@flow
import React from 'react';
import { Mutation } from 'react-apollo';
import { PERFILESQUERY, DELETE_PERFIL } from './queries';

const DeleteAlumno = (id) => {
  return <div> 
      <Mutation mutation={DELETE_PERFIL}>
            {(deleteAlumnos, { data }) => (
                <button onClick={() => 
                    deleteAlumnos({
                        variables: id, refetchQueries: [{ query: PERFILESQUERY }] 
                       })
                }>x</button>
            )}
        </Mutation>
  </div>;
};

export default (DeleteAlumno);