//@flow
import React from 'react';
import { Mutation } from 'react-apollo';
import { ALUMNOSQUERY, } from '../alumnos/queries';
import { ADD_PERFIL, ESCUELAS, GRUPO, PERFILESQUERY, GET_PERFIL_BY_USUARIOID } from './queries';
import { useQuery } from '@apollo/react-hooks';

const AddPerfil = ({ id, clave, nombreGrupo, grado, disabled }) => {
    const {
        called: calledEscuela,
        loading: loadingEscuela,
        data: dataEscuela,
    } = useQuery(ESCUELAS, { variables: { clave: clave } })
    const {
        called: calledGrupo,
        loading: loadingGrupo,
        data: dataGrupo,
    } = useQuery(GRUPO, { variables: { nombre: nombreGrupo } })
    const {
        called: calledPerfil,
        loading: loadingPerfil,
        data: dataPerfil,
    } = useQuery(GET_PERFIL_BY_USUARIOID, { variables: { id: id } })
    return <div>
        <Mutation mutation={ADD_PERFIL}>
            {(addPerfil, { data }) => {
                if (loadingPerfil) {
                    return "Loading...";
                } else {
                    return <button style={dataPerfil.perfil.length !== 0 ? { backgroundColor: 'red' } : { backgroundColor: 'green' }} disabled={dataPerfil.perfil.length !== 0} onClick={() => {
                        addPerfil({
                            variables: {
                                escuelaId: dataEscuela.escuelas[0].id,
                                gradoId: grado,
                                grupoId: dataGrupo.grupos[0].id,
                                usuarioId: id,
                            }, refetchQueries: [{
                                query: PERFILESQUERY
                            }]
                        })
                    }
                    }>profile</button>
                }

            }}
        </Mutation>
    </div>;
};

export default (AddPerfil);